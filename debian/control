Source: rust-oxilangtag
Section: rust
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-rust,
 librust-criterion-0.5+default-dev,
 librust-serde-test-1+default-dev,
Maintainer: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/rust-oxilangtag.git
Vcs-Browser: https://salsa.debian.org/debian/rust-oxilangtag
Homepage: https://github.com/oxigraph/oxilangtag
Rules-Requires-Root: no

Package: librust-oxilangtag-dev
Architecture: all
Multi-Arch: foreign
Depends:
 librust-serde-1+default-dev,
 ${misc:Depends},
Provides:
 librust-oxilangtag-0.1+default-dev (= ${binary:Version}),
 librust-oxilangtag-0.1+serialize-dev (= ${binary:Version}),
 librust-oxilangtag-0.1+std-dev (= ${binary:Version}),
 librust-oxilangtag-0.1-dev (= ${binary:Version}),
 librust-oxilangtag-0.1.5-dev (= ${binary:Version}),
Description: language tag normalization and validation - Rust source code
 OxiLangTag is a Rust library
 allowing to validate and normalize language tags
 following RFC 5646 (BCP 47).
 .
 It is a fork of "language-tags" focusing on RDF use cases.
 You might find the "language-tags" crate more convenient.
 .
 Resource Description Framework (RDF)
 is a standard model for data interchange on the Web.
 .
 This package contains the source for the Rust oxilangtag crate,
 for use with cargo.
